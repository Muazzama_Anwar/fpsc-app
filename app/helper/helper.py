import sys
from json import load, dump
import os

path = os.getcwd()

first_arg = sys.argv[1]
second_arg = sys.argv[2]


def parse_json(filename=first_arg, new_name=second_arg):
    updated_format = []
    with open(path+"/"+filename, 'r') as data:
        for d in load(data):
            print(d)
            updated_data = {}
            updated_data['question'] = d['question']
            updated_data['A'] = d['answer'][0]
            updated_data['B'] = d['answer'][1]
            updated_data['C'] = d['answer'][2]
            updated_data['D'] = d['answer'][3]
            updated_data['correct'] = d['correct']
            updated_format.append(updated_data)
        with open(path + "/" + new_name, 'w') as data:
            dump(updated_format, data)


if __name__ == "__main__":
    parse_json()
