from app import app
from flask import Response
import json
import os

path = os.getcwd()


@app.route("/")
def index():
    return Response(json.dumps({"message": "Welcome to FPCS model papers app."}), status=200, mimetype='application/json')


@app.route("/category")
def category():
    with open(path+"/app/assets/categories.json", 'r') as data:
        return Response(json.dumps({"categories": json.load(data)}), status=200, mimetype='application/json')


@app.route("/category/<cat>")
def model_papers(cat):
    with open(path+"/app/assets/categories.json", 'r') as data:
        for record in json.load(data):
            if cat == record['category']:
                return Response(json.dumps(record['papers']), status=200, mimetype='application/json')
            else:
                continue
        return Response(json.dumps({'message': "category not found"}), status=400, mimetype='application/json')


@app.route("/category/<category>/<paper>")
def paper(category, paper):
    with open(path+"/app/assets/categories.json", 'r') as data:
        result = next((item for item in json.load(data) if item["category"] == category), False)
        if result:
            rec = [x['model'] for x in result['papers']['response']]
            if paper in rec:
                for x in result['papers']['response']:
                    if x['model'] == paper:
                        data_file = path + x['path']
                        with open(data_file, 'r') as data:
                            return Response(json.dumps({"response": json.load(data)}), status=200, mimetype='application/json')
                    else:
                        continue
            else:
                return Response(json.dumps({'message': "model paper not found"}), status=400, mimetype='application/json')
        else:
            return Response(json.dumps({'message':"category not found"}), status=400, mimetype='application/json')
